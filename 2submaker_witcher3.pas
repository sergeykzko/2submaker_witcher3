uses sysutils;
type
  Aas = Array of AnsiString;
var
  source, destination : AnsiString;
  s, s_b : AnsiString;
  destination_data : Aas;
  m : LongWord = 0;
  n : LongWord;
  id : LongWord;
  NFinS : Aas;
{$I lib.pas}
begin
  if (paramstr(1) = '') or (paramstr(2) = '')
    then begin
      writeln;
      writeln('Usage: > 2submaker_witcher3 source_file.w3strings.csv destination_file.w3strings.csv');
      writeln;
      writeln('2submaker_witcher3 by SeaJay Greeny');
      writeln('https://github.com/seajay-greeny/2submaker_witcher3');
      writeln;
      writeln('2submaker has a Pinkie Pie superpower!  (Likely most.)');
      halt(1);
    end;
  if not fileExists(paramstr(1))
    then begin
      writeln('File'+ paramstr(1)+' not found.');
      halt(1);
    end;
  if not fileExists(paramstr(2))
    then begin
      writeln('File'+ paramstr(2)+' not found.');
      halt(1);
    end;
  source := paramstr(1);
  destination := paramstr(2);
  writeln('"',source,'" >> "',destination,'"');
  write('Loading "',destination,'"...');
  ArrayloadFromTxt(destination_data, destination);
  writeln(' Done!');
  writeln(length(destination_data),' strings loaded from "', destination,'"');
  repeat
    s := destination_data[m];
    if s[1] <> ';'
      then begin
        id := getIDFromW3strTableStr(s);
        s_b := getStringFromId(id, source);
        if s_b = ''
          then begin
            writeln('[ID ',id,'] : not found in "',source,'"');
            arrayAdd(NFinS, '[ID '+IntToStr(id)+'] : not found in "'+source+'"');
          end
          else begin
            writeln('[ID ',id,'] : ++', s_b);
            destination_data[m] := destination_data[m] + ' (' + s_b + ')';
          end;
      end;
    m := m + 1;
  until m = length(destination_data);
  //for n := 0 to length(destination_data) - 1 do writeln(destination_data[n]);
  if not DirectoryExists('out') then mkdir('out');
  write('Saving to "out/',destination,'"...');
  ArraySaveToTxt(destination_data, 'out/'+destination);
  writeln(' Done!');
  writeln(length(destination_data),' strings saved.');
  if length(NFinS) > 0
    then begin
      writeln(length(NFinS) ,'identifiers not found in the source.');
      for n := 0 to length(NFinS) - 1 do writeln(NFinS[n]);
    end;
  writeln;
  writeln ('Enjoy Carbon Johnson!');
  writeln ('Mr. Pearce is watching for you.');
end.
